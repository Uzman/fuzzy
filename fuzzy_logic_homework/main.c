//
//  main.c
//  fuzzy_logic_homework
//
//  Created by Ali Oğuz Uzman on 07/12/14.
//  Copyright (c) 2014 Oğuz Uzman. All rights reserved.
//  No: 1358110049

#include <stdio.h>

typedef enum  m{
    VeryHot,
    Hot,
    Normal,
    Cold,
    VeryCold
} Temperature ;

typedef enum mm{
    NotBlowing,
    NormalBlow,
    StrongBlow,
    VeryStrongBlow
    
}blow;

int _veryHotTemperature = 32;
int _hotTemperature = 26;
int _coldTemperature = 22;
int _veryColdTemperature = 16;


Temperature getTemperatureLevel(int temperature);

char* getBlowString(blow blowLevel);
int getBlowTemperature(Temperature temp);

int main(int argc, const char * argv[]) {
    // insert code here...
   
    Temperature FirstSensor, SecondSensor;
    Temperature OutputTemperature;
    blow OutputBlow;
    
    printf("program bir sonsuz dongu halinde calisacaktir.\n");
    
    
    while (1) {
        int first;
        int second;
        printf("Sensorun sicaklikliklarini girin:\n");
        scanf("%d %d",&first, &second);
        
        FirstSensor = getTemperatureLevel(first);
        SecondSensor = getTemperatureLevel(second);
        
        if(FirstSensor == VeryHot){
            
            if(SecondSensor == VeryHot){
                
                OutputBlow = VeryStrongBlow;
                OutputTemperature = VeryCold;
                
            } else if (SecondSensor == Hot){
                
                OutputTemperature = Cold;
                OutputBlow = VeryStrongBlow;
                
            } else if (SecondSensor == Normal){
                
                OutputTemperature = Cold;
                OutputBlow = StrongBlow;
                
            } else if (SecondSensor == Cold){
                
                OutputTemperature = Cold;
                OutputBlow = NormalBlow;
                
            } else if (SecondSensor == VeryCold){//sıcaklıklar birbirini dengelediği için üflemez
                
                OutputBlow = NotBlowing;
            }
            
        }
        else if(FirstSensor == Hot){
            
            if(SecondSensor == VeryHot){
                
                OutputBlow = VeryStrongBlow;
                OutputTemperature = VeryCold;
                
            } else if (SecondSensor == Hot){
                
                OutputTemperature = VeryCold;
                OutputBlow = StrongBlow;
                
            } else if (SecondSensor == Normal){
                
                OutputTemperature = Cold;
                OutputBlow = StrongBlow;
                
            } else if (SecondSensor == Cold){
                
                OutputBlow = NotBlowing;
                
            } else if (SecondSensor == VeryCold){
                
                OutputTemperature=VeryHot;
                OutputBlow = NormalBlow;
                
            }
            
        }
        else if(FirstSensor == Normal){
            
            if(SecondSensor == VeryHot){
                
                OutputBlow = NormalBlow;
                OutputTemperature = VeryCold;
                
            } else if (SecondSensor == Hot){
                
                OutputTemperature = Cold;
                OutputBlow = NormalBlow;
                
            } else if (SecondSensor == Normal){// Oda koşulları normal, çalımasına gerek yok
                
                OutputBlow = NotBlowing;
                
            } else if (SecondSensor == Cold){
                
                OutputTemperature = Hot;
                OutputBlow = NormalBlow;
                
            } else if (SecondSensor == VeryCold){
                
                OutputTemperature = VeryHot;
                OutputBlow = StrongBlow;
            }
            
        }
        else if(FirstSensor == Cold){
            
            if(SecondSensor == VeryHot){
                
                OutputBlow = NormalBlow;
                OutputTemperature = Cold;
                
            } else if (SecondSensor == Hot){
                
                OutputTemperature = Cold;
                OutputBlow = NotBlowing;
                
            } else if (SecondSensor == Normal){
                
                OutputTemperature = Cold;
                OutputBlow = StrongBlow;
                
            } else if (SecondSensor == Cold){
                
                OutputTemperature = VeryHot;
                OutputBlow = NormalBlow;
                
            } else if (SecondSensor == VeryCold){//sıcaklıklar birbirini dengelediği için üflemez
                OutputTemperature = VeryHot;
                OutputBlow = NotBlowing;
            }
            
        }
        else if(FirstSensor == VeryCold){
            
            if(SecondSensor == VeryHot){
                
                OutputBlow = NotBlowing;
                
            } else if (SecondSensor == Hot){
                
                OutputTemperature = VeryHot;
                OutputBlow = NormalBlow;
                
            } else if (SecondSensor == Normal){
                
                OutputTemperature = VeryHot;
                OutputBlow = StrongBlow;
                
            } else if (SecondSensor == Cold){
                
                OutputTemperature = VeryHot;
                OutputBlow = NormalBlow;
                
            } else if (SecondSensor == VeryCold){//sıcaklıklar birbirini dengelediği için üflemez
                
                OutputBlow = VeryStrongBlow;
                OutputTemperature = VeryHot;
                            }
        }
        if(OutputBlow == NotBlowing){
            printf("Gerekli degil\n");
        }
        else{
            char * blowLevel = getBlowString(OutputBlow);
            int temp = getBlowTemperature(OutputTemperature);
            printf("Ufleme siddeti: %s\nUfleme Sicakligi: %d\n", blowLevel, temp);
        }
    }
    
    return 0;
}

Temperature getTemperatureLevel(int first){
    Temperature sensor;
    if(first < _coldTemperature){
        if(first < _veryColdTemperature){
            sensor = VeryCold;
        }
        else{
            sensor = Cold;
        }
    }
    else if(first > _hotTemperature){
        if(first > _veryHotTemperature){
            sensor = VeryHot;
        }
        else{
            sensor = Hot;
        }
    }

    else{
    
        sensor = Normal;

    }
    return sensor;
}


char* getBlowString(blow blowLevel){
    switch (blowLevel) {
        case NormalBlow:
            return "normal hizda ufleme";
        case StrongBlow:
            return "Hizli ufleme";
        case VeryStrongBlow:
            return "Cok hizli ufleme";
        default:
            return "error";
    }
    
}

int getBlowTemperature(Temperature temp){
    switch (temp) {
        case VeryHot:
            return _veryHotTemperature;
        case Hot:
            return _hotTemperature;
        case Cold:
            return _coldTemperature ;
        case VeryCold:
            return _veryColdTemperature;
            
        default:
            return -111;//hata
    }
}
